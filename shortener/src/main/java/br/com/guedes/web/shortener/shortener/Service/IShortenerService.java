package br.com.guedes.web.shortener.shortener.Service;

import br.com.guedes.web.shortener.shortener.DTO.ShortenerRequest;
import br.com.guedes.web.shortener.shortener.DTO.ShortenerResponse;
import org.springframework.stereotype.Service;


@Service
public interface IShortenerService {

	ShortenerResponse shortenerUrl(ShortenerRequest shortenerRequestDto) throws Exception;

	ShortenerResponse findUrlOriginal(String token);

	ShortenerResponse findCountView(String token);
}
