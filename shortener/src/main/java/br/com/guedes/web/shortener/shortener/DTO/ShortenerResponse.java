package br.com.guedes.web.shortener.shortener.DTO;

import br.com.guedes.web.shortener.shortener.Model.Shortener;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ShortenerResponse {

    private String urlOriginal;

    private String token;

    private Long countView;

}
