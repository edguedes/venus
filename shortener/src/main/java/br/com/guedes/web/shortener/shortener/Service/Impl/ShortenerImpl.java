package br.com.guedes.web.shortener.shortener.Service.Impl;

import br.com.guedes.web.shortener.exception.NotFoundException;
import br.com.guedes.web.shortener.shortener.DTO.ShortenerRequest;
import br.com.guedes.web.shortener.shortener.DTO.ShortenerResponse;
import br.com.guedes.web.shortener.shortener.Model.Shortener;
import br.com.guedes.web.shortener.shortener.Repository.IShortenerRepository;
import br.com.guedes.web.shortener.shortener.Service.IShortenerService;
import br.com.guedes.web.shortener.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ShortenerImpl implements IShortenerService {

	@Autowired
	private IShortenerRepository shortenerRepository;

	private Util util = new Util();


	@Override
	public ShortenerResponse shortenerUrl(ShortenerRequest shortenerRequestDto) throws Exception {

		util.isValidateUrlOriginal(shortenerRequestDto.getUrl());

		Optional<Shortener> shortenerOpt = shortenerRepository.findByUrlOriginal(shortenerRequestDto.getUrl());

		if(shortenerOpt.isPresent()) {
			return new ShortenerResponse(
					shortenerOpt.get().getUrlOriginal(),
					shortenerOpt.get().getToken(),
					shortenerOpt.get().getQtdAccess());
		}

		Shortener shortener = new Shortener().toEntity(shortenerRequestDto);

		shortenerRepository.save(shortener);

		return new ShortenerResponse(
				shortener.getUrlOriginal(),
				shortener.getToken(),
				shortener.getQtdAccess());
	}

	@Override
	public ShortenerResponse findUrlOriginal(String token) {

		Optional<Shortener> shortenerOpt = Optional.ofNullable(shortenerRepository.findByToken(token)
				.orElseThrow(NotFoundException::new));

		shortenerOpt.get().setQtdAccess(shortenerOpt.get().getQtdAccess() + 1);
		shortenerRepository.save(shortenerOpt.get());

		return new ShortenerResponse(
				shortenerOpt.get().getUrlOriginal(),
				shortenerOpt.get().getToken(),
				shortenerOpt.get().getQtdAccess());
	}

	@Override
	public ShortenerResponse findCountView(String token) {

		Optional<Shortener> shortenerOpt = Optional.ofNullable(shortenerRepository.findByToken(token)
				.orElseThrow(NotFoundException::new));

		shortenerOpt.get().setQtdAccess(shortenerOpt.get().getQtdAccess() + 1);
		shortenerRepository.save(shortenerOpt.get());

		return new ShortenerResponse(
				shortenerOpt.get().getUrlOriginal(),
				shortenerOpt.get().getToken(),
				shortenerOpt.get().getQtdAccess());
	}

}
