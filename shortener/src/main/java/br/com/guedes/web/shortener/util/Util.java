package br.com.guedes.web.shortener.util;

import br.com.guedes.web.shortener.exception.BadRequestException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.UUID;

public class Util {

    public void isValidateUrlOriginal(String texto) {

        if ((texto == null) || texto.length() > 1000 || "".equals(texto) ) {
             throw new BadRequestException("URL inválida");
        }
    }

    public String createToken(){
       return UUID.randomUUID().toString().replace("-", "");
    }
}
