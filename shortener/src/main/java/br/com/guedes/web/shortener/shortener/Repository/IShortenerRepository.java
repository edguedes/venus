package br.com.guedes.web.shortener.shortener.Repository;

import br.com.guedes.web.shortener.shortener.Model.Shortener;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface IShortenerRepository extends JpaRepository<Shortener, Long> {

    Optional<Shortener> findByToken(String token);

    Optional<Shortener> findByUrlOriginal(String url);

}
