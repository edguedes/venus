package br.com.guedes.web.shortener.shortener.Controller;

import br.com.guedes.web.shortener.shortener.DTO.ShortenerRequest;
import br.com.guedes.web.shortener.shortener.DTO.ShortenerResponse;
import br.com.guedes.web.shortener.shortener.Service.Impl.ShortenerImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;


@RestController
@RequestMapping(value = "api/v1")
public class ShortenerController {

	@Autowired
	private ShortenerImpl shortenerImpl;
	
	@PostMapping()
	@ResponseStatus(HttpStatus.CREATED)
	public ShortenerResponse shortenerUrl(@RequestBody ShortenerRequest shortenerRequestDto) throws Exception {
		return shortenerImpl.shortenerUrl(shortenerRequestDto);
	}

	@GetMapping(path = "/view/{token}")
	@ResponseStatus(HttpStatus.OK)
	public ShortenerResponse getUrlOriginal(@PathVariable String token) throws Exception {
		return shortenerImpl.findCountView(token);

	}

	@GetMapping(path = "/{token}")
	@ResponseStatus(HttpStatus.OK)
	public void findPath(@PathVariable String token, HttpServletResponse httpServletResponse) throws Exception {
		ShortenerResponse resp = shortenerImpl.findUrlOriginal(token);
		httpServletResponse.sendRedirect(resp.getUrlOriginal());
	}
}
