package br.com.guedes.web.shortener.shortener.Model;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

import br.com.guedes.web.shortener.shortener.DTO.ShortenerRequest;
import br.com.guedes.web.shortener.util.Util;
import com.fasterxml.jackson.databind.util.BeanUtil;
import lombok.*;
import org.springframework.beans.BeanUtils;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="tb_shortener")
public class Shortener implements Serializable{

	@Id
	@Column(name="id_shortener")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="id_shortener")
	private Long id;

	@NotBlank
	@Column(unique=true, length=1000)
	private String urlOriginal;

	@NotBlank
	private LocalDateTime dateCriation;

	private Long qtdAccess;

	@Column(unique=true)
	private String token;

	public Shortener toEntity(ShortenerRequest shortenerRequest){
		Util util = new Util();

		Shortener shortener = new Shortener();
		BeanUtils.copyProperties(shortenerRequest, shortener);

		shortener.setUrlOriginal(shortenerRequest.getUrl());
		shortener.setDateCriation(LocalDateTime.now());
		shortener.setQtdAccess(0L);
		shortener.setToken(util.createToken());

		return  shortener;
	}
}

