package br.com.guedes.web.shortener.shortener.DTO;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import br.com.guedes.web.shortener.shortener.Model.Shortener;
import br.com.guedes.web.shortener.util.Util;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ShortenerRequest {
	
	private String url;

}
