package br.com.guedes.web.shortener.util;

import br.com.guedes.web.shortener.exception.BadRequestException;
import br.com.guedes.web.shortener.shortener.Model.Shortener;
import br.com.guedes.web.shortener.shortener.Repository.IShortenerRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;
import java.util.Random;

public class ShortenerConvert {

    private static final String RANDOM_ALPHABET = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    private static final long BASE = RANDOM_ALPHABET.length();

    @Autowired
    private IShortenerRepository shortenerRepository;

    public String createCode(Long id) {
            if (id != null) {
                StringBuilder shortenKey = new StringBuilder();
                while (id > 0) {
                    shortenKey.insert(0, RANDOM_ALPHABET.charAt((int) (id % BASE)));
                    id = id / BASE;
                    return shortenKey.toString();
                }
            }
        throw new BadRequestException();
    }

    public long decode(String shortenKey) {
        long num = 0;
        for (int i = 0; i < shortenKey.length(); i++) {
            num = num * BASE + RANDOM_ALPHABET.indexOf(shortenKey.charAt(i));
        }
        return num;
    }
}
